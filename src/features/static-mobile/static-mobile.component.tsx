import * as React from 'react'
import './static-mobile.css'
import logo from '../../assets/hilton-logo.png'
import hilton from '../../assets/hotelexterior.jpg'

export const StaticMobileComponent = () => {
  return (
    <div className='mobile-container'>
      <div className='header d-flex  justify-content-between align-items-center'>
        <button>{"<"} Back</button>
        <h4>Hotel Details</h4>
        <img className='logo' src={logo} />
      </div>
      <img className='hiltonexterior' src={hilton} />
      <div className='details'>
        <h5>Hilton Chicago</h5>
        <p>720 South Michigan Avenue</p>
        <p>Chicago, Illinois, 60605</p>
        <a href="tel:13129224400">1-312-922-4400</a>
      </div>
      <div className='nav-mobile'>
        <ul>
          <li>Map <i>></i></li>
          <li>Photo gallery <i>></i></li>
          <li>Amenities <i>></i></li>
          <li>Some Other Link <i>></i></li>
        </ul>
      </div>
    </div>
  )
}