import * as React from 'react'
import './assessment-splash.css'

export const AssessmentSplashComponent = () => {
  return (
    <div className='splash-container'>
      <h3>Assessment 1 Documentation</h3>
      <h6><strong>Assumptions</strong></h6>
      <ul>
        <li>Page is built entirely as html so it can be utilized solo in a React app or not.</li>
        <li>Some styles are off for fonts, some clarification on fonts would allow to have proper matching to design.</li>
        <li>If the app needs to be componentized for react the seperate sections can be split off easily enough.</li>
      </ul>
      <h3>Assessment 2 Documentation</h3>
      <h6><strong>Statement</strong></h6>
      <p>After chatting with Ryan about making sure there are differences from Aric's setup I went with a slightly non-standard setup that moves all business logic to the reducer. This is a layout I have been experimenting with that helps contain where business logic is implemented, and makes it derived from the store. I usually go with more standard approaches based on project needs and the team I would be working with.</p>
      <h6><strong>Assumptions</strong></h6>
      <ul>
        <li>Site is for Consumers so is built for responsive layout, just requires a little more styling from Design to accomadte different sizes. (Making them just a fixed width is easy enough to switch if necessary for Assessment)</li>
        <li>There is no listed way in the requirements to allow for removing a cached set of Room data. An easy way to achieve this would be a reset or clear data button.</li>
        <li>Some standard stylings for are used based on browser, specific custom inputs can be added as necessary based on user environments</li>
      </ul>
      <h6><strong>Exceptions</strong></h6>
      <ul>
        <li>There is not 100% code coverage for tests, but i wanted to show the route it could take with Container Components and the store.</li>
      </ul>
    </div>
  )
}

