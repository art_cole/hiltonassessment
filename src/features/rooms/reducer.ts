import * as roomsActions from './actions'
import { IRooms } from './rooms.model'
import { IRoom, Room } from './components/room/room.model'

export const initialState: IRooms = {hasExisting:  false, rooms: [new Room({active: true, adults: 1, children: 0}), new Room, new Room, new Room]}

export function reducer (state: IRooms = initialState, action: any) {
  switch (action.type) {
    case roomsActions.ActionTypes.LOAD_PERSISTED_ROOMS:
      let existingRooms = localStorage.getItem('rooms')
      if (existingRooms !== null) {
        return {
          hasExisting: true,
          rooms: JSON.parse(existingRooms)
        }
      } else {
        return {
          hasExisting: false,
          ...state
        }
      }
    case roomsActions.ActionTypes.UPDATE_ROOM:
      return updateToRooms(state, action.payload)
    case roomsActions.ActionTypes.SAVE_ROOMS:
      localStorage.setItem('hasExisting', JSON.stringify(true))
      localStorage.setItem('rooms', JSON.stringify(state.rooms))
      return {
        ...state
      }
    default:
      return {
        ...state
      }
  }
}

function updateToRooms(state: IRooms, payload: {room: IRoom, index: number}) {
  let currentRooms: Room[] = [...state.rooms]
  currentRooms[payload.index] = payload.room

  if (payload.index === 1 && !currentRooms[payload.index].active) {
      currentRooms[1] = new Room()
      currentRooms[2] = new Room()
      currentRooms[3] = new Room()
  }

  if (payload.index === 2 && !currentRooms[payload.index].active) {
    currentRooms[2] = new Room()
    currentRooms[3] = new Room()
  } else if (payload.index === 2 && currentRooms[payload.index].active) {
    currentRooms[1].active = true
  }

  if (payload.index === 3 && currentRooms[payload.index].active) {
    currentRooms[1].active = true
    currentRooms[2].active = true
  } else if (payload.index === 3 && !currentRooms[payload.index].active) {
    currentRooms[3] = new Room()
  }

  return {
    hasExisting: true,
    rooms: currentRooms
  }
}