import { initialState, reducer } from './reducer'

test('Rooms Reducer initial state', () => {
  expect(reducer(initialState, {Type: 'test'} as any)).toEqual(initialState)
})