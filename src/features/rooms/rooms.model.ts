import { IRoom, Room } from './components/room/room.model'

export interface IRooms {
  hasExisting: boolean
  rooms: IRoom[]
}

export class RoomsModel implements IRooms {
  public hasExisting: boolean
  public rooms: IRoom[]

  constructor(reqData?: IRooms) {
    this.hasExisting = reqData && reqData.hasExisting || false
    this.rooms = reqData && reqData.rooms || [new Room({active: true, adults: 1, children: 0}), new Room, new Room, new Room]
  }
}