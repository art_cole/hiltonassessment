import * as React from 'react'
import { connect } from 'react-redux'
import { IAppState } from '../../store/app.reducer'
import { RoomComponent } from './components/room/room.component'
import { IRoom } from './components/room/room.model'
import './rooms.css'
import {loadPersistedRooms, saveRooms, updateRoom} from './actions'

interface IProps {
  hasExisting: boolean
  rooms: IRoom[]

  loadPersistedRooms(): void
  saveRooms(): void
  updateRoom (room: IRoom, index: number): void
}

export class RoomsComponent extends React.Component<IProps, object> {
  readonly state = {
    rooms: this.props.rooms
  }

  componentDidMount() {
    if (localStorage.getItem('hasExisting')) {
      this.props.loadPersistedRooms()
    }
  }

  componentWillReceiveProps(nextProps: IProps) {
    if (nextProps.rooms && nextProps.rooms !== this.state.rooms) {
      this.setState({rooms: nextProps.rooms})
    }
  }

  handleRoomUpdate = (room: IRoom, index: number) => {
    this.props.updateRoom(room, index)
  }

  public render() {
    const rooms = this.state.rooms.map((room: IRoom, index) =>
      <div key={index} className='col col-sm-3'>
        <RoomComponent room={room} index={index} roomUpdate={this.handleRoomUpdate}/>
      </div>
    )

    return (
      <div className='row rooms-container'>
        {rooms}
        <div className='col col-sm-12'>
          <button onClick={this.props.saveRooms}>Submit</button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: IAppState) => {
  return {
    hasExisting: state.rooms.hasExisting,
    rooms: state.rooms.rooms
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    loadPersistedRooms: () => { dispatch(loadPersistedRooms()) },
    saveRooms: () => { dispatch(saveRooms()) },
    updateRoom: (room: IRoom, index: number) => { dispatch(updateRoom(room, index)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomsComponent)