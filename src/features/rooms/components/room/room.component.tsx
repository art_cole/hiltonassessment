import * as React from 'react'
import { IRoom } from './room.model'
import './room.css'

export const RoomComponent = (props: {room: IRoom, index: number, roomUpdate: any}) => {
  let primaryClass = 'room-container '
  if (props.index !== 0 && props.room.active || props.index === 0) {
    primaryClass += 'active'
  }

  return (
    <div className={primaryClass}>
      <h4>
        {props.index !== 0 &&
          <input
            type='checkbox'
            checked={props.room.active}
            onChange={(e) => props.roomUpdate({...props.room, active: e.target.checked}, props.index)}
          />
        }
        Room {props.index + 1}
      </h4>
      <div className='d-flex justify-content-around'>
        <div className='p-2'>
          <label>Adults<br/>(+18)</label>
          <br/>
          <select disabled={!props.room.active} value={props.room.adults} onChange={(e) => props.roomUpdate({...props.room, adults: e.target.value}, props.index)}>
            <option value={1}>1</option>
            <option value={2}>2</option>
          </select>
        </div>
        <div className='p-2'>
          <label>Children<br/>(0-18)</label>
          <br/>
          <select disabled={!props.room.active} value={props.room.children} onChange={(e) => props.roomUpdate({...props.room, children: e.target.value}, props.index)}>
            <option value={0}>0</option>
            <option value={1}>1</option>
            <option value={2}>2</option>
          </select>
        </div>
      </div>
    </div>
  )
}