export interface IRoom {
  active: boolean
  adults: number
  children: number
}

export class Room implements IRoom {
  public active: boolean
  public adults: number
  public children: number

  constructor(reqData?: IRoom) {
    this.active = reqData && reqData.active || false
    this.adults = reqData && reqData.adults || 1
    this.children = reqData && reqData.children || 0
  }
}