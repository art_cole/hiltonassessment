import * as React from 'react'
import { shallow } from 'enzyme'
import { RoomsComponent } from './rooms.component'

test('Component Rendered', () => {
  const wrapper = shallow(<RoomsComponent
    hasExisting={false}
    loadPersistedRooms={jest.fn()}
    rooms={[]}
    saveRooms={jest.fn()}
    updateRoom={jest.fn()}
  />)
  expect(wrapper).toBeTruthy()
})

test('Rooms ComponentDidMount existing rooms', () => {
  const spy = jest.fn()
  localStorage.setItem('hasExisting', 'true')
  const wrapper = shallow(<RoomsComponent
    hasExisting={false}
    loadPersistedRooms={spy}
    rooms={[]}
    saveRooms={jest.fn()}
    updateRoom={jest.fn()}
  />)
  const instance = wrapper.instance() as any
  instance.componentDidMount()
  expect(spy).toBeCalled()
})

test('Rooms Component componentWillRecieveProps', () => {
  const wrapper = shallow(<RoomsComponent
    hasExisting={false}
    loadPersistedRooms={jest.fn()}
    rooms={[]}
    saveRooms={jest.fn()}
    updateRoom={jest.fn()}
  />)
  const instance = wrapper.instance() as any
  instance.componentWillReceiveProps({})
  expect(wrapper.state('rooms')).toEqual([])
})

test('Rooms Component handleRoomUpdate', () => {
  const spy = jest.fn()
  const wrapper = shallow(<RoomsComponent
    hasExisting={false}
    loadPersistedRooms={jest.fn()}
    rooms={[]}
    saveRooms={jest.fn()}
    updateRoom={spy}
  />)
  const instance = wrapper.instance() as any
  instance.handleRoomUpdate({active: true, adults: 1, children: 0}, 0)
  expect(spy).toBeCalledWith({active: true, adults: 1, children: 0}, 0)
})