import { IRoom } from './components/room/room.model'

export const ActionTypes = {
  LOAD_PERSISTED_ROOMS: '[Rooms] Load Persisted Rooms Data',
  SAVE_ROOMS: '[Rooms] Save Rooms Data',
  UPDATE_ROOM: '[Rooms] Update Room'
}

export function updateRoom (room: IRoom, index: number) {
  return {type: ActionTypes.UPDATE_ROOM, payload: {room, index}}
}

export function loadPersistedRooms () {
  return {type: ActionTypes.LOAD_PERSISTED_ROOMS}
}

export function saveRooms () {
  return {type: ActionTypes.SAVE_ROOMS}
}