import { routerForBrowser } from 'redux-little-router'
import { AssessmentSplashComponent } from './features/assessment-splash/assessment-splash.component'
import RoomsComponent from './features/rooms/rooms.component'
import { StaticMobileComponent } from './features/static-mobile/static-mobile.component'

export const routes = {
  '/': {
    component: AssessmentSplashComponent,
    title: 'Home'
  },
  '/rooms': {
    component: RoomsComponent,
    title: 'Assessment 2'
  },
  '/static-mobile': {
    component: StaticMobileComponent,
    title: 'Assessment 1'
  }
}

export const { reducer, middleware, enhancer } = routerForBrowser({
  routes
})