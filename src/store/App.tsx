import * as React from 'react';
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { Fragment } from 'redux-little-router'
import * as router from '../routes'
import { NavComponent } from '../shared/components/navigation/nav.component'
import { appReducers } from './app.reducer'

const store = createStore(
  appReducers,
  composeWithDevTools(
    router.enhancer,
    applyMiddleware(router.middleware)
  )
)

export default class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
          <NavComponent>
            <Fragment forRoute='/'>
              <div className='col col-xs-12 '>
                {Object.keys(router.routes).map((route, i) => (
                  <Fragment key={i} forRoute={route}>
                    {React.createElement(router.routes[route].component)}
                  </Fragment>
                ))}
              </div>
            </Fragment>
          </NavComponent>
      </Provider>
    );
  }
}
