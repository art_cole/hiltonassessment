import { combineReducers } from 'redux'
import { Router } from 'redux-little-router'
import { reducer as roomsReducer } from '../features/rooms/reducer'
import { IRooms } from '../features/rooms/rooms.model'
import { reducer as routerReducer } from '../routes'

export interface IAppState {
  rooms: IRooms,
  router: Router
}

export const appReducers = combineReducers({
    rooms: roomsReducer,
    router: routerReducer
})