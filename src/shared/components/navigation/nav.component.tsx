import * as React from 'react'
import { Link } from 'redux-little-router'
import './nav.css'

export const NavComponent = (props: {children?: any }) => {
  return (
    <div className='container-fluid'>
      <div className="row justify-content-md-center nav-container align-items-center">
        <div className="col col-sm-2">
          <Link href='/'>Documentation</Link>
        </div>
        <div className="col col-sm-2">
          <Link href='/static-mobile'>Assessment 1</Link>
        </div>
        <div className="col col-sm-2">
          <Link href='/rooms'>Assessment 2</Link>
        </div>
      </div>
      <div className='row'>
        {props.children}
      </div>
    </div>
  )
}